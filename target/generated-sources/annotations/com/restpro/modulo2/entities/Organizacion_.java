package com.restpro.modulo2.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2018-01-16T19:49:42")
@StaticMetamodel(Organizacion.class)
public class Organizacion_ { 

    public static volatile SingularAttribute<Organizacion, Integer> idEmpresa;
    public static volatile SingularAttribute<Organizacion, String> cargo;
    public static volatile SingularAttribute<Organizacion, String> nombreEmpresa;

}