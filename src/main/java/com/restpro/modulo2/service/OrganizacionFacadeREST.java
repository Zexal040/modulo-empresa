/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.restpro.modulo2.service;

import com.restpro.modulo2.entities.Organizacion;
import java.util.List;
import javax.ejb.Stateless;
import javax.json.Json;
import javax.json.JsonObject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Zexal_000
 */
@Stateless
@Path("empresa")
public class OrganizacionFacadeREST extends AbstractFacade<Organizacion> {

    //@PersistenceContext(unitName = "com.RestPro_Modulo2_war_1.0PU")
    private EntityManager em;
    private String JSON_RESPONSE = "{\"Id\":%d, \"Operación\":\"%s\", \"Resultado\":\"%s\"}";

    public OrganizacionFacadeREST() {
        super(Organizacion.class);
    }

    @POST
    @Override
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public String create(Organizacion entity) {
        String resultado = super.create(entity);
        return String.format(JSON_RESPONSE, entity.getIdEmpresa(), "INSERT", resultado);
    }

                    
    @PUT            
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)

    public String edit(@PathParam("id") Integer id, Organizacion entity) {
        String resultado = super.edit(entity);
        return String.format(JSON_RESPONSE, entity.getIdEmpresa(), "UPDATE", resultado);
    }

    /**
     * Método para eliminar por medio de la id un registro
     *
     * @param id recibe el paramatro que se desea eliminar
     * @return una respuesta en formato JSON de la petición utilizada
     */
    @DELETE
    @Path("{id}")
    public String remove(@PathParam("id") Integer id) {
        String resultado = super.remove(super.find(id));
        String mensaje = "{\"Id\":%d, \"Operación\":\"%s\", \"Resultado\":\"%s\"}";
        return String.format(mensaje, id, "DELETE", resultado);
    }

    @GET
    @Path("{id}")
    @Produces({"application/json"})
    public Organizacion find(@PathParam("id") Integer id) {
        return super.find(id);
    }

    @GET
    @Override
    @Produces({"application/json"})
    public List<Organizacion> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/json"})
    public List<Organizacion> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        if(em==null){
            EntityManagerFactory factory = Persistence.createEntityManagerFactory("com.RestPro_Modulo2_war_1.0PU");
            em = factory.createEntityManager();
        }
        return em;
    }
    
}
