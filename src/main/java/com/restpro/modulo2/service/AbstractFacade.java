/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.restpro.modulo2.service;

import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Zexal_000
 */
public abstract class AbstractFacade<T> {

    private Class<T> entityClass;

    public AbstractFacade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    protected abstract EntityManager getEntityManager();

  public String create(T entity) {
        try {
            EntityManager em = getEntityManager();
            em.getTransaction().begin();
            em.persist(entity);
            em.getTransaction().commit();
            em.refresh(entity);
            return "TODO OK";
        } catch (Exception e) {
            return e.getLocalizedMessage();
        }
 
    }

    /**
     * Método para indicar que se actualizó de manera adecuada un registro
     * @param entity hace una instancia persistente y actualiza el estado de la
     * base de datos
     * @return un mensaje satisfactorio sí es que la transacción fue exitosa
     */
    
    public String edit(T entity) {
        try {
            EntityManager em = getEntityManager();
            em.getTransaction().begin();
            em.merge(entity);
            em.getTransaction().commit();
            return "TODO OK";
        } catch (Exception e) {
            return e.getLocalizedMessage();
        }
        
    }

    /**
     * Método para indicar que se eliminó de manera adecuada un registro
     * @param entity hace una instancia persistente y actualiza el estado de la
     * base de datos
     * @return un mensaje satisfactorio sí es que la transacción fue exitosa
     */
    
    public String remove(T entity) {
        try {
            EntityManager em = getEntityManager();
            em.getTransaction().begin();
            em.remove(em.merge(entity));
            em.getTransaction().commit();
            return "TODO OK";
        } catch (Exception e) {
            return e.getLocalizedMessage();
        }
    }

    public T find(Object id) {
        return getEntityManager().find(entityClass, id);
    }

    public List<T> findAll() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return getEntityManager().createQuery(cq).getResultList();
    }

    public List<T> findRange(int[] range) {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        q.setMaxResults(range[1] - range[0] + 1);
        q.setFirstResult(range[0]);
        return q.getResultList();
    }

    public int count() {
        javax.persistence.criteria.CriteriaQuery cq = getEntityManager().getCriteriaBuilder().createQuery();
        javax.persistence.criteria.Root<T> rt = cq.from(entityClass);
        cq.select(getEntityManager().getCriteriaBuilder().count(rt));
        javax.persistence.Query q = getEntityManager().createQuery(cq);
        return ((Long) q.getSingleResult()).intValue();
    }
    
}
