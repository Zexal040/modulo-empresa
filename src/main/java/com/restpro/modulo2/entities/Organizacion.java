/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.restpro.modulo2.entities;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.apache.log4j.Logger;

/**
 *
 * @author Zexal_000
 */
@Entity
@Table(name = "organizacion", catalog = "empresa", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Organizacion.findAll", query = "SELECT o FROM Organizacion o")
    , @NamedQuery(name = "Organizacion.findByIdEmpresa", query = "SELECT o FROM Organizacion o WHERE o.idEmpresa = :idEmpresa")
    , @NamedQuery(name = "Organizacion.findByNombreEmpresa", query = "SELECT o FROM Organizacion o WHERE o.nombreEmpresa = :nombreEmpresa")
    , @NamedQuery(name = "Organizacion.findByCargo", query = "SELECT o FROM Organizacion o WHERE o.cargo = :cargo")})
public class Organizacion implements Serializable {

    private static Logger log = Logger.getLogger(Organizacion.class);
    
    public static void main(String[] args)
{
    if (log.isTraceEnabled())
    {
        log.trace("mensaje de trace");
    }

    if (log.isDebugEnabled())
    {
        log.debug("mensaje de debug");
    }

    if (log.isInfoEnabled())
    {
        log.info("mensaje de info");
    }
    
    log.warn("mensaje de warn");
    log.error("mensaje de error");
    log.fatal("mensaje de fatal");
}
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_empresa", nullable = false)
    private Integer idEmpresa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "nombre_empresa", nullable = false, length = 100)
    private String nombreEmpresa;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "cargo", nullable = false, length = 100)
    private String cargo;

    public Organizacion() {
    }

    public Organizacion(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public Organizacion(Integer idEmpresa, String nombreEmpresa, String cargo) {
        this.idEmpresa = idEmpresa;
        this.nombreEmpresa = nombreEmpresa;
        this.cargo = cargo;
    }

//<editor-fold defaultstate="collapsed" desc="GETTERS">
    public Integer getIdEmpresa() {
        return idEmpresa;
    }
    
    public String getNombreEmpresa() {
        return nombreEmpresa;
    }
    
    public String getCargo() {
        return cargo;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="SETTERS">
    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }
    
    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }
    
    public void setCargo(String cargo) {
        this.cargo = cargo;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="HASH - EQUALS">
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(this.idEmpresa);
        hash = 37 * hash + Objects.hashCode(this.nombreEmpresa);
        hash = 37 * hash + Objects.hashCode(this.cargo);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Organizacion other = (Organizacion) obj;
        if (!Objects.equals(this.nombreEmpresa, other.nombreEmpresa)) {
            return false;
        }
        if (!Objects.equals(this.cargo, other.cargo)) {
            return false;
        }
        if (!Objects.equals(this.idEmpresa, other.idEmpresa)) {
            return false;
        }
        return true;
    }
//</editor-fold>
   
}
